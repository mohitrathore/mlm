<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Transactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class WithdrawController extends Controller
{
    public function index()
    {   $user = User::where('email',Auth::user()->email)->first();
        $withdraws = DB::table('withdraw')->whereNotIn('email',[Auth::user()->email])->paginate(12);

    	return view('admin.withdraw')->with(compact('withdraws','user'));
    }
    public function approve($id)
    {
    	$request=DB::table('withdraw')->where('id',$id)->update(['status'=>1]);
    	$email = DB::table('withdraw')->where('id',$id)->first();
         $user = User::where('email',$email->email)->first();
         $earning=$user->earning - $email->amount;
    	 if($request)
            {   
                $update=User::where('email',$email->email)->update(['earning'=>$earning]);

				$Transaction=new Transactions;
				$Transaction->email=$email->email;
				$Transaction->transaction_id='TRANS'.mt_rand(1000000, 9999999);
				$Transaction->purpose='Withdraw';
				$Transaction->amount=$email->amount;
				$Transaction->created_at=now();
				$Transaction->save();

            return redirect()->back()->with('success','Request Status Change to Approved Successfully');
            }
            else
            {
            return redirect()->back()->with('error','Request Status Change to Approved Error');
            }
    }
    public function pending($id)
    {
    	$requestDeny=DB::table('withdraw')->where('id',$id)->update(['status'=>0]);

    	 if($requestDeny)
            {
            return redirect()->back()->with('success','Request Status Change to Pending Successfully');
            }
            else
            {
            return redirect()->back()->with('error','Request Status Change to Pending Error');
            }
    }
    public function denied($id)
    {
    	 $requestDeny=DB::table('withdraw')->where('id',$id)->update(['status'=>2]);

    	 if($requestDeny)
            {
            return redirect()->back()->with('success','Request Status Change to Denied Successfully');
            }
            else
            {
            return redirect()->back()->with('error','Request Status Change to Denied Error');
            }
    }
}
