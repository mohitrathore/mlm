<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Image;

class ProfileController extends Controller
{
     public function index()
     {  
     	 $user = User::where('email',Auth::user()->email)->first();
     	 $MemberUsers = User::whereNotIn('email',[Auth::user()->email])->where('status',1)->get();
         $MemberUsercount=count($MemberUsers);
     	return view('admin.profile')->with(compact('user','MemberUsercount'));
     }
     public function update(Request $request)
     {
        $request->validate([

            'name'=>'required|max:200',
            'mobile'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10',
            'profileImage'=> 'mimes:jpg,jpeg,png',
            'address'=> 'required|max:400'
            ]);

			if ($profileimage = $request->file('profileImage')) 
			{ 
                  $ProfileImage= 'u'.date('YmdHis') . "." .$profileimage->getClientOriginalName();
				$image_resize = Image::make($profileimage->getRealPath());              
				$image_resize->resize(128, 128);
				$image_resize->save('ProfileImages/' .$ProfileImage);

				if($request->input('oldprofileImage') !== null)
				{
				unlink('ProfileImages/'.$request->input('oldprofileImage'));
				}			
			}   // New Profile Image Upload
			else
			{
			$ProfileImage=$request->input('oldprofileImage');
			}



			$update=User::where('email',Auth::user()->email)
            ->update([
            'name'=>$request->input('name'),
            'profileImage'=>$ProfileImage,
            'mobile'=>$request->input('mobile'),
            'address'=>$request->input('address'),
            'updated_at'=>now()
            ]);

            if($update)
            {
            return redirect()->route('admin.profile')->with('success','Profile Updated Successfully');
            }
            else
            {
            return redirect()->back()->with('error',' Profile Updation Error');
            }
     }
}
