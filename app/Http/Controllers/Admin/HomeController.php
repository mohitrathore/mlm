<?php
 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Transactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
     
     public function index()
     {   

     	 $user = User::where('email',Auth::user()->email)->first();
         $transactions = Transactions::orderBy('id', 'DESC')->paginate(10);
         $withdrawal = DB::table('withdraw')->orderBy('id', 'desc')->take(5)->get();
         $members =User::whereNotIn('email',[Auth::user()->email])->orderBy('id','desc')->where('status',1)->take(10)->get();

     	 $RegisteredUsers = User::whereNotIn('email',[Auth::user()->email])->get();
     	 $RegisteredUsercount=count($RegisteredUsers);
     	 $MemberUsers = User::whereNotIn('email',[Auth::user()->email])->where('status',1)->get();
     	 $MemberUsercount=count($MemberUsers);
     	 

     	 if($user->level == 0)
        {
          Session::put('level_name','Level 0');  
        }
        else
        {
         $level=DB::table('levels')->select('name')->where('level',$user->level)->first();
        Session::put('level_name',$level->name);
        }

     	return view('admin.dashboard')->with(compact('user','RegisteredUsercount','MemberUsercount','transactions','withdrawal','members'))->with('no',1);
     }
    
}
