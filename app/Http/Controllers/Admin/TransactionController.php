<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transactions;
class TransactionController extends Controller
{
    public function index()
     {    
     	 $user = User::where('email',Auth::user()->email)->first();
     	 $transactions = Transactions::paginate(15);
     	 return view('admin.transaction')->with(compact('user','transactions'));
     }
}
