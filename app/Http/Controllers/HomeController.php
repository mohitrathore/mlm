<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Accounts;
use App\Transactions;
use Illuminate\Support\Facades\Auth;
use Razorpay\Api\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = User::where('email',Auth::user()->email)->first();
        $referralusers=User::where('referral_by',Auth::user()->email)->paginate(20);
        $withdrawal=DB::table('withdraw')->where('email',Auth::user()->email)->where('status',1)->sum('amount');

         //level 1 count
        $referralpersonalcount=count($referralusers);
        $level1data=$referralusers->pluck('email');
       
        $allreferral_count=count($level1data);
        //level 2 count
        $level2query=User::whereIn('referral_by',$level1data)->get();
        $level2data=$level2query->pluck('email');
        $allreferral_count+=count($level2data);
        //level 3 count
        $level3query=User::whereIn('referral_by',$level2data)->get();
        $level3data=$level3query->pluck('email');
        $allreferral_count+=count($level3data);
        //level 4 count
        $level4query=User::whereIn('referral_by',$level3data)->get();
        $level4data=$level4query->pluck('email');
        $allreferral_count+=count($level4data);
        //level 5 count
        $level5query=User::whereIn('referral_by',$level4data)->get();
        $level5data=$level5query->pluck('email');
        $allreferral_count+=count($level5data);
        //level 6 count
        $level6query=User::whereIn('referral_by',$level5data)->get();
        $level6data=$level6query->pluck('email');
        $allreferral_count+=count($level6data);
        //level 7 count
        $level7query=User::whereIn('referral_by',$level6data)->get();
        $level7data=$level7query->pluck('email');
        $allreferral_count+=count($level7data);
        //level 8count
        $level8query=User::whereIn('referral_by',$level7data)->get();
        $level8data=$level8query->pluck('email');
        $allreferral_count+=count($level8data);
        //level 9 count
        $level9query=User::whereIn('referral_by',$level8data)->get();
        $level9data=$level9query->pluck('email');
        $allreferral_count+=count($level9data);
        
         
        if($user->level == 0)
        {
          Session::put('level_name','user');  
        }
        else
        {
         $level=DB::table('levels')->select('name')->where('level',$user->level)->first();
        Session::put('level_name',$level->name);
        }
        
        if($user->status == 0 || $user->status == null)
        {
            $api = new Api('rzp_live_rk8040rquAcPfF', 'EdpBdo1KmObO0AxH0jDD6369');
            $order  = $api->order->create(array('receipt' => Auth::user()->name, 'amount' => 2100 * 100 , 'currency' => 'INR')); // Creates order
           
             return view('user.home')->with(compact('user','order'));
        }

        return view('user.home')->with(compact('user','referralpersonalcount','referralusers','allreferral_count','withdrawal'));

    } 
    
    public function mobile_notverified()
    {   
        if(Auth::user()->otp_verified_at !== null)
        {
           return redirect()->route('home'); 
        }
        else{
          return view('user.mobileverified');  
        }
        
    }

    public function mobile_verified(Request $request)
    {
         
        $userfind=User::where('mobile','=',$request->mobile)->first();
      
        if($request->otp =="" || $request->otp ==" " || $request->otp ==null)
        {
        return redirect()->back()->with('error','OTP Found Blank, Verification failed !');
        }
        else if($userfind->otp == $request->input('otp'))
        {
              
            $user=User::where('mobile',$request->input('mobile'))
            ->update([
            'otp'=>null,
            'otp_verified_at'=>now(),
            'updated_at'=>now()
            ]);
            if($user)
            {
              return redirect()->back()->with('success','Mobile Verification Done Successfully');
            }
            else
            {
               return redirect()->back()->with('error','Mobile Verification Failed');
            }
        }
        else
        {
              return redirect()->back()->with('error','Entered OTP is not match, Please Resend OTP Again');
        }
      
    }
    
        public function sendOtp(Request $request)
        {
       
        $otp = rand(1000,9999);
       
        $user = User::where('mobile','=',$request->mobile)->where('email','=',Auth::user()->email)->update(['otp' => $otp]);
       
        if($user == 1)
        {
            $authKey = "313372656478736d733830331613632715";

            // mobiles numbers 
            $mobileNumber = $request->mobile;

            //Sender ID,While using route4 sender id should be 6 characters long.
            $senderId = "TESTSM";

            //Your message to send, Add URL encoding here.
            $message = "Your Accure Vision Account Verification OTP is".' '.$otp;

            //Define route 
            $route = "4";
            //Prepare you post parameters
            $postData = array(
            'authentic-key' => $authKey,
            'number' => $mobileNumber,
            'message' => $message,
            'senderid' => $senderId,
            'route' => $route
            );

            //print_r($postData);
            //Put your sms provider api
            $url="http://sms.redxsms.com/http-tokenkeyapi.php?";

            // init the resource
            $ch = curl_init();

            curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
            ));


            //Ignore SSL certificate verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


            //get response
            $output = curl_exec($ch);



            //Print error if any
            if(curl_errno($ch))
            {

            echo 'error:' . curl_error($ch);
            }

            curl_close($ch); 
            return response()->json([1,'OTP Send to your registered mobile number'],200);
            }
            else if($user == 0)
            {
              return response()->json([0,'This mobile number is not register with your account OR mobile number not found !'],200);  
            }
            
    }
    /* Payment method*/
     protected function razorpay(Request $request)
          {
          $data = $request->all();

          
          $api = new Api('rzp_live_rk8040rquAcPfF', 'EdpBdo1KmObO0AxH0jDD6369');
          try{
          $attributes = array(
          'razorpay_signature'  => $data['razorpay_signature'],
          'razorpay_payment_id' => $data['razorpay_payment_id'],
          'razorpay_order_id'   => $data['razorpay_order_id']
          );
          $order = $api->utility->verifyPaymentSignature($attributes);
          $success = true;
        
          }catch(SignatureVerificationError $e){

          $succes = false;
         

          }

        if($success)
        {  
            $update=User::where('email',Auth::user()->email)->update(['status'=>1]);
            /* After Payment newly register user status change to activated */

            /* Level 1 */
            $RegistrationTransaction=new Transactions;
            $RegistrationTransaction->email=Auth::user()->email;
            $RegistrationTransaction->transaction_id='TRANS'.mt_rand(1000000, 9999999);
            $RegistrationTransaction->purpose='Registration Amount';
            $RegistrationTransaction->amount=2100;
            $RegistrationTransaction->created_at=now();
            $RegistrationTransaction->save();
            //Registration Amount for current register users

            $currentuser=User::where('email',Auth::user()->email)->first();
            $level1_referralBy=$currentuser->referral_by;
           
            if(!empty($level1_referralBy))
            {   
                $Referral=new Transactions;
                $Referral->email=$level1_referralBy;
                $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                $Referral->purpose='Referral';
                $Referral->amount=600;
                $Referral->created_at=now();
                $Referral->save();
               //Referral Amount

                $level1referaluser=User::where('email',$level1_referralBy)->first();
                $level1_referralcount=$level1referaluser->referral_count + 1;
                //Referral count update level1  
                $level1_referralcodeuse=User::where('email',$level1_referralBy)->where('status',1)->get();
                $level1percentagecount=round(count($level1_referralcodeuse) * 60 /100);

                if($level1percentagecount >= 5)
                {
                $level1_referralearning=$level1referaluser->earning + $level1referaluser->earning_block + 600;
                $level1_updatereferralcount=User::where('email',$level1_referralBy)->update(['referral_count'=>$level1_referralcount,'earning'=>$level1_referralearning,'earning_block'=>0,'level'=>1]); 
                }
                else
                {
                $level1_referralearning=$level1referaluser->earning_block + 600;
                $level1_updatereferralcount=User::where('email',$level1_referralBy)->update(['referral_count'=>$level1_referralcount,'earning_block'=>$level1_referralearning]); 
                }
                /* Level 2 */
                $level1user=User::where('email',$level1_referralBy)->first();
                $level2email=$level1user->referral_by;

                if(!empty($level2email))
                {   
                    $Referral=new Transactions;
                    $Referral->email=$level2email;
                    $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                    $Referral->purpose='Referral';
                    $Referral->amount=200;
                    $Referral->created_at=now();
                    $Referral->save();
                    //Referral Amount
                    $check_level2=User::where('email',$level2email)->where('status',1)->get();
                    $level2percentagecount=round(count($check_level2) * 60 /100);
                    $level2user=User::where('email',$level2email)->first();
                    if($level2percentagecount >= 25)
                    {

                    $level2_referralearning=$level2user->earning +$level2user->earning_block + 200;
                    $level2_updatereferral=User::where('email',$level2email)->update(['earning'=>$level2_referralearning,'earning_block' => 0,'level'=>2]);
                    /* earning update*/
                    }
                    else
                    {
                    $level2_referralearning=$level2user->earning_block + 200;
                    $level2_updatereferral=User::where('email',$level2email)->update(['earning_block'=>$level2_referralearning]);
                    /* blocked earning update*/
                    }

                    /* Level 3 */

                    $level3_referralcodeuse=User::where('email',$level2email)->first();
                    $level3email=$level3_referralcodeuse->referral_by;
                 

                    if(!empty($level3email))
                    {   
                            $Referral=new Transactions;
                            $Referral->email=$level3email;
                            $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                            $Referral->purpose='Referral';
                            $Referral->amount=80;
                            $Referral->created_at=now();
                            $Referral->save();
                            //Referral Amount
                        $check_level3=User::where('email',$level3email)->where('status',1)->get();
                        
                        $level3percentagecount=round(count($check_level3) * 60 /100);
                        $level3user=User::where('email',$level3email)->first();

                        if($level3percentagecount >= 125)
                        {
                        $level3_referralearning=$level3user->earning +$level3user->earning_block + 80;
                        $level3_updatereferral=User::where('email',$level3email)->update(['earning'=>$level3_referralearning,'earning_block' => 0,'level'=>3]);
                        /* earning update level 3*/
                        }
                        else
                        {
                        $level3_referralearning=$level3user->earning_block + 80;
                        $level3_updatereferral=User::where('email',$level3email)->update(['earning_block'=>$level3_referralearning]);
                        /* blocked earning update level 3*/
                        }

                        /* Level 4 */
                        $level4_referralcodeuse=User::where('email',$level3email)->first();
                        $level4email=$level4_referralcodeuse->referral_by;

                        if(!empty($level4email))
                        {
                            $Referral=new Transactions;
                            $Referral->email=$level4email;
                            $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                            $Referral->purpose='Referral';
                            $Referral->amount=32;
                            $Referral->created_at=now();
                            $Referral->save();
                            //Referral Amount
                            $check_level4=User::where('email',$level4email)->where('status',1)->get();
                            
                            $level4percentagecount=round(count($check_level4) * 60 /100);
                            $level4user=User::where('email',$level4email)->first();

                            if($level4percentagecount >= 625)
                            {

                            $level4_referralearning=$level4user->earning +$level4user->earning_block + 32;
                            $level4_updatereferral=User::where('email',$level4email)->update(['earning'=>$level4_referralearning,'earning_block' => 0,'level'=>4]);
                            /* earning update level4*/
                            }
                            else
                            {
                            $level4_referralearning=$level4user->earning_block + 32;
                            $level4_updatereferral=User::where('email',$level4email)->update(['earning_block'=>$level4_referralearning]);
                            /* blocked earning update level 4*/
                            }
                            /* Level 5 */
                            $level5_referralcodeuse=User::where('email',$level4email)->first();
                            $level5email=$level5_referralcodeuse->referral_by;

                            if(!empty($level5email))
                            {   
                                $Referral=new Transactions;
                                $Referral->email=$level5email;
                                $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                                $Referral->purpose='Referral';
                                $Referral->amount=16;
                                $Referral->created_at=now();
                                $Referral->save();
                                //Referral Amount
                                $check_level5=User::where('email',$level5email)->where('status',1)->get();
                                $level5percentagecount=round(count($check_level5) * 60 /100);
                                $level5user=User::where('email',$level5email)->first();
                               
                                if($level5percentagecount >= 3125)
                                {

                                $level5_referralearning=$level5user->earning +$level5user->earning_block + 16;
                                $level5_updatereferral=User::where('email',$level5email)->update(['earning'=>$level5_referralearning,'earning_block' => 0,'level'=>5]);
                                /* earning update level 5*/
                                }
                                else
                                {
                                $level5_referralearning=$level5user->earning_block + 16;
                                $level5_updatereferral=User::where('email',$level5email)->update(['earning_block'=>$level5_referralearning]);
                                /* blocked earning update level 5*/
                                }
                                 /* Level 6 */
                                $level6_referralcodeuse=User::where('email',$level5email)->first();
                                $level6email=$level6_referralcodeuse->referral_by;

                                if(!empty($level6email))
                                {   
                                    $Referral=new Transactions;
                                    $Referral->email=$level6email;
                                    $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                                    $Referral->purpose='Referral';
                                    $Referral->amount=3.2;
                                    $Referral->created_at=now();
                                    $Referral->save();
                                    //Referral Amount
                                    $check_level6=User::where('email',$level6email)->where('status',1)->get();
                                    $level6percentagecount=round(count($check_level6) * 60 /100);
                                   $level6user=User::where('email',$level6email)->first();                                   

                                    if($level6percentagecount >= 15625)
                                    {

                                    $level6_referralearning=$level6user->earning +$level6user->earning_block + 3.2;
                                    $level6_updatereferral=User::where('email',$level6email)->update(['earning'=>$level6_referralearning,'earning_block' => 0,'level'=>6]);
                                    /* earning update level 6*/
                                    }
                                    else
                                    {
                                    $level6_referralearning=$level6user->earning_block + 3.2;
                                    $level6_updatereferral=User::where('email',$level6email)->update(['earning_block'=>$level6_referralearning]);
                                    /* blocked earning update level 6*/
                                    }
                                    /* Level 7 */
                                    $level7_referralcodeuse=User::where('email',$level6email)->first();
                                    $level7email=$level7_referralcodeuse->referral_by;

                                    if(!empty($level7email))
                                    {   
                                        $Referral=new Transactions;
                                        $Referral->email=$level7email;
                                        $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                                        $Referral->purpose='Referral';
                                        $Referral->amount=12.8;
                                        $Referral->created_at=now();
                                        $Referral->save();
                                        //Referral Amount

                                        $check_level7=User::where('email',$level7email)->where('status',1)->get();
                                        $level7percentagecount=round(count($check_level7) * 60 /100);
                                        $level7user=User::where('email',$level7email)->first();
                                        if($level7percentagecount >= 78125)
                                        {

                                        $level7_referralearning=$level7user->earning +$level7user->earning_block + 12.8;
                                        $level7_updatereferral=User::where('email',$level7email)->update(['earning'=>$level7_referralearning,'earning_block' => 0,'level'=>7]);
                                        /* earning update level 7*/
                                        }
                                        else
                                        {
                                        $level7_referralearning=$level7user->earning_block + 12.8;
                                        $level7_updatereferral=User::where('email',$level7email)->update(['earning_block'=>$level7_referralearning]);
                                        /* blocked earning update level 7*/
                                        }
                                        /* Level 8 */
                                        $level8_referralcodeuse=User::where('email',$level7email)->first();
                                        $level8email=$level8_referralcodeuse->referral_by;

                                        if(!empty($level8email))
                                        {   

                                            $Referral=new Transactions;
                                            $Referral->email=$level8email;
                                            $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                                            $Referral->purpose='Referral';
                                            $Referral->amount=7.68;
                                            $Referral->created_at=now();
                                            $Referral->save();
                                            //Referral Amount
                                            $check_level8=User::where('email',$level8email)->where('status',1)->get();
                                            $level8percentagecount=round(count($check_level8) * 60 /100);
                                            $level8user=User::where('email',$level8email)->first();
                                            if($level8percentagecount >= 390625)
                                            {

                                            $level8_referralearning=$level8user->earning +$level8user->earning_block + 7.68;
                                            $level8_updatereferral=User::where('email',$level8email)->update(['earning'=>$level8_referralearning,'earning_block' => 0,'level'=>8]);
                                            /* earning update level 8*/
                                            }
                                            else
                                            {
                                            $level8_referralearning=$level8_referralcodeuse->earning_block + 7.68;
                                            $level8_updatereferral=User::where('email',$level8email)->update(['earning_block'=>$level8_referralearning]);
                                            /* blocked earning update level 8*/
                                            }
                                              /* Level 9 */
                                        $level9_referralcodeuse=User::where('email',$level8email)->first();
                                        $level9email=$level9_referralcodeuse->referral_by;

                                        if(!empty($level9email))
                                        {   

                                            $Referral=new Transactions;
                                            $Referral->email=$level9email;
                                            $Referral->transaction_id='TRANS'.mt_rand(1000000, 9999999);
                                            $Referral->purpose='Referral';
                                            $Referral->amount=5.12;
                                            $Referral->created_at=now();
                                            $Referral->save();
                                            //Referral Amount
                                            $check_level9=User::where('email',$level9email)->where('status',1)->get();
                                            $level9percentagecount=round(count($check_level9) * 60 /100);
                                            $level9user=User::where('email',$level9email)->first();
                                            if($level9percentagecount >= 1953125)
                                            {

                                            $level9_referralearning=$level9user->earning +$level9user->earning_block + 5.12;
                                            $level9_updatereferral=User::where('email',$level9email)->update(['earning'=>$level9_referralearning,'earning_block' => 0,'level'=>9]);
                                            /* earning update level 9*/
                                            }
                                            else
                                            {
                                            $level9_referralearning=$level9user->earning_block + 5.12;
                                            $level9_updatereferral=User::where('email',$level9email)->update(['earning_block'=>$level9_referralearning]);
                                            /* blocked earning update level 9*/
                                            }
                                        }
                                      }
                                    }
                                }
                            }
                        }
                    }

                }

            }

            else
            {

            }
            return redirect()->route('home')->with('success','Registration Fees Submited Successfully. Now you can Refer User from your account');
        }
        else
        {

        return redirect()->route('home')->with('error','Payment Failed ! Check your details again Or Try again ?');
        }
      }
}
