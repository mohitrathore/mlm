<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Accounts;
use Illuminate\Support\Facades\Session;
use Image;
class ProfileController extends Controller
{
    public function index()
    {   
        $user = User::where('email',Auth::user()->email)->first();
        $account = Accounts::where('email',Auth::user()->email)->first();

    	return view('user.profile')->with(compact('user','account'));
    }

    public function profileupdate(Request $request)
    {
    	$request->validate([

            'name'=>'required|max:200',
            'mobile'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10',
            'profileImage'=> 'mimes:jpg,jpeg,png',
            'address'=> 'required|max:400'
            ]);

			if ($profileimage = $request->file('profileImage')) 
			{ 
                  $ProfileImage= 'u'.date('YmdHis') . "." .$profileimage->getClientOriginalName();
				$image_resize = Image::make($profileimage->getRealPath());              
				$image_resize->resize(128, 128);
				$image_resize->save('ProfileImages/' .$ProfileImage);

				if($request->input('oldprofileImage') !== null)
				{
				unlink('ProfileImages/'.$request->input('oldprofileImage'));
				}			
			}   // New Profile Image Upload
			else
			{
			$ProfileImage=$request->input('oldprofileImage');
			}



			$update=User::where('email',Auth::user()->email)
            ->update([
            'name'=>$request->input('name'),
            'profileImage'=>$ProfileImage,
            'mobile'=>$request->input('mobile'),
            'address'=>$request->input('address'),
            'updated_at'=>now()
            ]);

            if($update){

            return redirect()->route('profile.index')->with('success','Profile Updated Successfully');
            }else{
            return redirect()->back()->with('error',' Profile Updation Error');
            }
    }
    public function account(Request $request)
    {
    	$request->validate([

            'holder_name'=>'required',
            'ac'=>'required|regex:/^\d{9,18}$/',
            'email'=>'unique:bankaccounts',
            'bank_name'=> 'required',
            'ifsc'=> 'required|regex:/^[A-Za-z]{4}\d{7}$/'
            ]);

			$account =Accounts::updateOrCreate([
			'email'   =>Auth::user()->email,
			],[
			'email' => Auth::user()->email,
			'bankholder_name'=>$request->input('holder_name'),
			'account_number'=>$request->input('ac'),
			'bank_name'=>$request->input('bank_name'),
			'ifsc_code'=>$request->input('ifsc'),
			'updated_at'=>now()
			]);

        if($account){

            return redirect()->route('profile.index')->with('success','Bank Account Details  Change Successfully');
            }else{
            return redirect()->back()->with('error','Bank Account Details Change Error');
            }
    }
}
