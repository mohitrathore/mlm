<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Transactions;
class TransactionController extends Controller
{
     
     public function index()
     {    
     	 $user = User::where('email',Auth::user()->email)->first();
     	 $transactions = Transactions::where('email',Auth::user()->email)->paginate(15);
     	 return view('user.transaction')->with(compact('user','transactions'));
     }
}
