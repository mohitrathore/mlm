<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Razorpay\Api\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
class ReferralController extends Controller
{
     public function index()
    {   
        $user = User::where('email',Auth::user()->email)->first();
        $referralusers=User::where('referral_by',Auth::user()->email)->where('status',1)->get();

        //level 1 count
        $referralpersonalcount=count($referralusers);
        $level1data=$referralusers->pluck('email');
       
        $allreferral_count=count($level1data);
        //level 2 count
        $level2query=User::whereIn('referral_by',$level1data)->where('status',1)->get();
        $level2data=$level2query->pluck('email');
        $allreferral_count+=count($level2data);          
        //$merged = $level1data->merge($level2data);
        //level 3 count
        $level3query=User::whereIn('referral_by',$level2data)->where('status',1)->get();
        $level3data=$level3query->pluck('email');
        $allreferral_count+=count($level3data);
        //level 4 count
        $level4query=User::whereIn('referral_by',$level3data)->where('status',1)->get();
        $level4data=$level4query->pluck('email');
        $allreferral_count+=count($level4data);
        //level 5 count
        $level5query=User::whereIn('referral_by',$level4data)->where('status',1)->get();
        $level5data=$level5query->pluck('email');
        $allreferral_count+=count($level5data);
        //level 6 count
        $level6query=User::whereIn('referral_by',$level5data)->where('status',1)->get();
        $level6data=$level6query->pluck('email');
        $allreferral_count+=count($level6data);
        //level 7 count
        $level7query=User::whereIn('referral_by',$level6data)->where('status',1)->get();
        $level7data=$level7query->pluck('email');
        $allreferral_count+=count($level7data);
        //level 8count
        $level8query=User::whereIn('referral_by',$level7data)->where('status',1)->get();
        $level8data=$level8query->pluck('email');
        $allreferral_count+=count($level8data);
        //level 9 count
        $level9query=User::whereIn('referral_by',$level8data)->where('status',1)->get();
        $level9data=$level9query->pluck('email');
        $allreferral_count+=count($level9data);

        $referrals = $level1data->concat($level2data)->concat($level3data)->concat($level4data)->concat($level5data)->concat($level6data)->concat($level7data)->concat($level8data)->concat($level9data);
        $allreferralss=$referrals->toArray();

           $allreferrals_selection=User::whereIn('email',$referrals)->where('status',1)->get();
           $allreferralusers=User::whereIn('email',$allreferralss)->where('status',1)->paginate(20);

        return view('user.referral')->with(compact('user','allreferralusers','allreferrals_selection'));

    }
    public function find(Request $request)
    {
        if($request->ajax())
			{     
				if($request->referralid !== 0 )
				{
					$referralBy=User::where('id',$request->referralid)->first();
					$referralsGet=User::where('referral_by',$referralBy->email)->get();
					$view = view("user.findreferrals",compact('referralsGet'))->render();

					return response()->json(['html'=>$view]);
				}
				
			}
    }
}
