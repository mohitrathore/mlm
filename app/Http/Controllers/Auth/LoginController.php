<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
      public function redirectTo()
    {
        switch(Auth::user()->role){
        case 'admin':
        $this->redirectTo ='Ur0HOxTCCViZRbFd/dashboard';
        return $this->redirectTo;
        break;
       
        case 'user':
        $this->redirectTo = '/home';
        return $this->redirectTo;
        break;

        /*default:
        $this->redirectTo = url()->previous();
        return $this->redirectTo;*/
        }
         return $next($request);
    }
   // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {   
        $input = $request->all();
  
        $this->validate($request, [
            'mobile' => 'required',
            'password' => 'required',
        ]);
  
        $fieldType = 'mobile';
        if(auth()->attempt(array($fieldType => $input['mobile'], 'password' => $input['password'])))
        {
            return redirect()->route('home');
        }else{
            return redirect()->route('login')
                ->with('error','Mobile Number And Password Are Wrong.');
        }
          
    }
}
