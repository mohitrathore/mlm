<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class WithdrawController extends Controller
{
    public function index()
    {   $user = User::where('email',Auth::user()->email)->first();
        $withdraw = DB::table('withdraw')->where('email',Auth::user()->email)->paginate(12);

    	return view('user.withdraw')->with(compact('withdraw','user'));
    }
    public function request(Request $request)
    {   
    	$user = User::where('email',Auth::user()->email)->first();
        $withdraw = DB::table('withdraw')->where('email',Auth::user()->email)->where('status',0)->get();
      
        if($request->input('amount') > $user->earning)
        {
             return redirect()->back()->with('error','Your Requested amount is not be higher then your balance');
        }
        else if($user->earning > 3000 && $request->input('amount') >= 3000)
    	{

            if(count($withdraw) > 0)
            {
             return redirect()->back()->with('error','Sorry, You cannot put a new request when one of your withraw request is in pending mode');
            }
            else
            {
            $request->validate([
            'amount'=>'required|numeric',
            ]);

            $withdraw=DB::table('withdraw')->insert
            ([ 

            'email'=>Auth::user()->email,
            'amount'=>$request->input('amount'),
            'transaction_id'=>'TRANS'.mt_rand(1000000, 9999999),
            'created_at'=>now(),
            ]);

            if($withdraw){

            return redirect()->route('withdraw.index')->with('success','Withdraw Request Submited Successfully');
            }else{
            return redirect()->back()->with('error',' Withdraw Request Submit Error');
            }  
            }
			
    	}
    	else
    	{
    		 return redirect()->back()->with('error','Withdraw Request minimum amount is 3000 Rupees');
    	}

    	
    }

}
