<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class MobileVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (!Auth::check()) {
        return redirect()->route('login');
        }

        if (Auth::user()->role == 'user') {
          
            if (Auth::user()->otp_verified_at !== null) {
                
                return $next($request);  
            }
            else
            {
             return redirect()->route('mobile_not_verified');
            }
           
        }
    }
}
