<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
	protected $table = "bankaccounts";
	protected $fillable = ['email','bankholder_name','account_number','bank_name','ifsc_code'];
	protected $hidden = ['account_number','bank_name','ifsc_code'];

}
