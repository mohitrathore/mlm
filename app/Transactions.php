<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = "transaction";
	protected $fillable = ['email','transaction_id','purpose','amount'];
	protected $hidden = ['email','transaction_id','purpose','amount'];
}
