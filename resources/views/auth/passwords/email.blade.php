<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Login | Accure Vision</title>
    <link href="{{asset('user_dashboard_css/css/dashboard.css')}}" rel="stylesheet" />
  </head>
  <body class="">
    <div class="page">
      <div class="page-single">
        <div class="container">
         <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <img src="{{asset('user_dashboard_css/images/logo.png')}}" class="h-8" alt="">
              </div>
           
              @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
        <form class="card" action="{{ route('password.email') }}" method="post">
                @csrf
                
                <div class="card-body p-6">
                  <div class="card-title">{{ __('Reset Password') }}</div>

                  <div class="form-group">
                    <label class="form-label">Email address</label>
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                 
                  <div class="form-footer">
                  <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </body>
   <!-- Session erros -->
  @if(session()->has('error'))
        <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
        </script>

        @endif 
</html>