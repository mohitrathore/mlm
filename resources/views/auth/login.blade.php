<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Login | Accure Vision</title>
    <link href="{{asset('user_dashboard_css/css/dashboard.css')}}" rel="stylesheet" />
  </head>
  <body class="">
    <div class="page">
      <div class="page-single">
        <div class="container">
         <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <img src="{{asset('user_dashboard_css/images/logo.png')}}" class="h-8" alt="">
              </div>
           
@if(Session::get('registererror') !== null)
<p class="alert alert-danger">{{ Session::get('registererror')}}</p>

If you are not register then go <a href="{{route('register')}}" class="btn btn-info btn-sm ">Back to Register</a>
<br /><br />
@endif
<!-- Referal code not activated error   --><!-- Session erros -->
  @if(session()->has('error'))
   <span class="alert alert-danger">
   {{session()->get('error')}}
   </span>
  @endif 
  <br />
              <form class="card" action="{{ route('login') }}" method="post">
                @csrf
                <div class="card-body p-6">
                  <div class="card-title">Login to your account</div>

                  <div class="form-group">
                    <label class="form-label">Mobile Number</label>
                  <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" autofocus>

                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      Password
                   <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                    </label>
                  </div>
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                  </div>
                  <br />
                  <div class="form-group">
                  <a href="/password/reset" class="text-dark"> 
                      {{ __('Forgot Password') }}
                      </a>
                  </div>
                  
                </div>
              </form>

              <div class="text-center text-muted">
                Don't have account yet? <a href="{{route('register')}}">Sign up</a>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </body>
   <!-- Session erros -->
  @if(session()->has('error'))
        <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
        </script>

        @endif 
</html>