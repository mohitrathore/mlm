<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Register With Us | Accure Vision</title>
    
    <link href="{{asset('user_dashboard_css/css/dashboard.css')}}" rel="stylesheet" />
  </head>
  <body class="">
    <div class="page">
      <div class="page-single">
        <div class="container">
        <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-8">
                <img src="{{asset('user_dashboard_css/images/logo.png')}}" class="h-8" alt="">
              </div>
            <div class="card">
               <div class="card-header">Register With Us</div>
                    @if(session()->has('error'))
                   <span class="alert alert-danger">
                       {!! session()->get('error')!!}
                   </span>

                    @endif 

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                            <div class="form-group">
                            <label class="form-label">Name *</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus required>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            </div>
                            <div class="form-group">
                            <label class="form-label">Mobile *</label>
                            <input id="mobile" type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}"  autocomplete="mobile" autofocus required>

                            @error('mobile')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            </div>
                        <div class="form-group">
                           <label class="form-label">Email (optional)</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          
                        </div>

                        <div class="form-group">
                            <label class="form-label">{{ __('Password') }} *</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" required>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>

                        <div class="form-group">
                            <label class="form-label">{{ __('Confirm Password') }} *</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password" required>
                        </div>
                       <div class="form-group">
                           <label class="form-label">Referral ID *</label>
                                <input id="referral" type="text" class="form-control @error('referral') is-invalid @enderror" name="referral" value="{{ old('referral') }}"  autocomplete="referral" autofocus required>

                                @error('referral')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          
                        </div>
                        
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input class="form-check-input" type="checkbox" name="terms-conditions" id="terms-conditions" {{ old('terms-conditions') ? 'checked' : '' }} required>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Terms & Conditions') }}
                                    </label>
                    </label>
                  </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
              <div class="text-center text-muted">
                You have account? <a href="{{route('login')}}">Login</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>