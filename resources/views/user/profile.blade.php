@extends('user.layouts.app')

@section('content')
 @if($user->status == 0)
            <script>
            window.location.href="<?php echo route('home')?>";
            </script>
            @else
                <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              <div class="col-lg-4">
                
                <div class="card">
                  <div class="card-body">
                    <div class="media">

                      @if($user->profileImage !== null)
              
                    <span class="avatar avatar-xxl mr-5" style="background-image: url(../ProfileImages/{{$user->profileImage}})"></span>
                      @else
                       <i class="fe fe-user mr-5" style="font-size:40px;"></i>
                      @endif
                      <div class="media-body">
                        <h4 class="m-0 text-capitalize">{{$user->name}}</h4>
                        <p class="text-muted mb-0 text-capitalize">{{Session::get('level_name')}}</p>
                       
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title font-weight-bold">Bank Details</h3>
                  </div>
                  <div class="card-body">

                    <form method="post" action="{{route('profile.account')}}">
                      @csrf
                      <input type="hidden" name="cid" value="{{$user->id}}" />
                      @if(!empty($account))
                        <div class="form-group">
                        <label class="form-label">Bank Holder Name</label>
                        <input class="form-control text-capitalize" placeholder="John Doe" name="holder_name" value="{{$account->bankholder_name}}" type="text"/>
                        <span style="color:orange"> @error('holder_name') {{ $message }}@enderror </span>
                        </div>
                        <div class="form-group">
                        <label class="form-label">A/c Number</label>
                        <input class="form-control" placeholder="1234567891245780" name="ac" value="{{$account->account_number}}" type="number"/>
                        <span style="color:orange"> @error('ac') {{ $message }}@enderror </span>
                        </div>

                        <div class="form-group">
                        <label class="form-label">Bank Name</label>
                        <input class="form-control" placeholder="Bank Name" type="text" name="bank_name"value="{{$account->bank_name}}"/>
                        <span style="color:orange"> @error('bank_name') {{ $message }}@enderror </span>
                        </div>
                        <div class="form-group">
                        <label class="form-label">IFSC Code</label>
                        <input class="form-control" placeholder="IFSC Code" type="text" name="ifsc" value="{{$account->ifsc_code}}"/>
                        <span style="color:orange"> @error('ifsc') {{ $message }}@enderror </span>
                        </div>
                        @else
                        <div class="form-group">
                        <label class="form-label">Bank Holder Name</label>
                        <input class="form-control" placeholder="John Doe"  type="text" name="holder_name" />
                         <span style="color:orange"> @error('holder_name') {{ $message }}@enderror </span>
                      </div>
                      <div class="form-group">
                        <label class="form-label">A/c Number</label>
                        <input class="form-control" type="number" placeholder="1234567891245780" name="ac"/>
                          <span style="color:orange"> @error('ac') {{ $message }}@enderror </span>
                      </div>

                      <div class="form-group">
                        <label class="form-label">Bank Name</label>
                        <input class="form-control text-capitalize" type="text" placeholder="Bank Name" name="bank_name"/>
                          <span style="color:orange"> @error('bank_name') {{ $message }}@enderror </span>
                      </div>
                      <div class="form-group">
                        <label class="form-label">IFSC Code</label>
                        <input class="form-control" placeholder="IFSC Code" type="text" name="ifsc"/>
                          <span style="color:orange"> @error('ifsc') {{ $message }}@enderror </span>
                      </div>
                        @endif
                      
                      <div class="alert alert-info" role="alert">
                    Please check your all details again before proceed...
                    <br />These details are use for withdraw your earning !
                    </div>
                      <div class="form-footer">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                      </div>
                    </form>

                  </div>
                </div>
              </div>
              <div class="col-lg-8">
             
                <form class="card" method="post" enctype="multipart/form-data" action="{{route('profile.update')}}">
                  @csrf
                  <div class="card-body">
                    <h3 class="card-title">Edit Profile</h3>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Name</label>
                          <input type="text" class="form-control" name="name" placeholder="Name" value="{{$user->name}}">

                          <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Mobile</label>
                          <input type="text" class="form-control" name="mobile" placeholder="Mobile" value="{{$user->mobile}}">
                          <span style="color:orange"> @error('mobile') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-7">
                        <div class="form-group">
                          <label class="form-label">Email address</label>
                          <input type="email" class="form-control" name="email" placeholder="Email" value="{{$user->email}}" disabled="disabled">
                        </div>
                      </div>

                      <div class="col-sm-6 col-md-5">
                        <div class="form-group">
                          <label class="form-label">Profile Picture</label>
                          <input type="file" class="form-control" name="profileImage" placeholder="Image">
                        </div>
                      </div>
                      
                     
                     
                      <div class="col-md-12">
                        <div class="form-group mb-0">
                          <label class="form-label">Address</label>
                          <textarea rows="5" class="form-control" name="address" placeholder="Here can be your address" >{{$user->address}}</textarea>
                          <span style="color:orange"> @error('address') {{ $message }}@enderror </span>
                        </div>
                      </div>
                       <input type="hidden" class="form-control" name="oldprofileImage" value="{{$user->profileImage}}"> 
                    </div>
                  </div>
                  <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Update Profile</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          @endif
        </div>
@endsection
