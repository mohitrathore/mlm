 <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <title>{{ config('app.name', 'Accure Vision') }}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="./assets/js/require.min.js"></script>
   
    <!-- Dashboard Core -->
        <link href="{{asset('user_dashboard_css/css/dashboard.css')}}" rel="stylesheet" />
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
        <!-- c3.js Charts Plugin -->
        <link href="{{asset('user_dashboard_css/plugins/charts-c3/plugin.css')}}" rel="stylesheet" />
        <script src="{{asset('user_dashboard_css/plugins/charts-c3/plugin.js')}}"></script>
        <!--Toastr CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
  </head>
 
  <body class="">
    <div class="page">
      <div class="page-main">
        <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="./index.html">
                <img src="{{asset('user_dashboard_css/images/logo.png')}}" class="header-brand-img" alt="Accure Vision">
              </a>
              <div class="d-flex order-lg-2 ml-auto">
              
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    @if($user->profileImage !== null)
              
                    <span class="avatar" style="background-image: url(../ProfileImages/{{$user->profileImage}})"></span>
                      @else
                       <i class="fe fe-user mr-5" style="font-size:40px;"></i>
                      @endif
                    <span class="ml-2 d-none d-lg-block">
                      <span class="text-default text-capitalize">{{Auth::user()->name}}</span>
                      <small class="text-muted d-block mt-1 text-capitalize">{{Session::get('level_name')}}</small>
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      @if($user->status !== 0)
                    <a class="dropdown-item" href="{{route('profile.index')}}">
                      <i class="dropdown-icon fe fe-user"></i> Profile
                    </a>
                    <a class="dropdown-item" href="{{route('withdraw.index')}}">
                      <i class="dropdown-icon fa fa-money"></i> Withdraw
                    </a>
                     <a class="dropdown-item" href="{{route('transaction')}}">
                      <i class="dropdown-icon fa fa-exchange"></i> Transaction History
                    </a>
                    @endif
                    <!-- Log Out -->
                    <a class="dropdown-item" href="{{ route('logout') }}" 
                    onclick="event.preventDefault(); 
                    document.getElementById('logout-form').submit();">  <i class="dropdown-icon fe fe-log-out"></i>{{ __('Logout') }} </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                    </form>

                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-3 ml-auto">
                <form class="input-icon my-3 my-lg-0">
                  <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search"></i>
                  </div>
                </form>
              </div>
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="{{route('home')}}" class="nav-link"><i class="fe fe-home"></i> Home</a>
                  </li>
                   @if($user->status !== 0)
          
                  <li class="nav-item">
                    <a href="{{route('referral.index')}}" class="nav-link"> <i class="fa fa-handshake-o"></i> 
                    &nbsp;Referrals</a>
                  </li>

                  <li class="nav-item">
                   <a class="nav-link" href="{{route('withdraw.index')}}">
                      <i class="fa fa-money"></i> Withdraw
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{route('transaction')}}" class="nav-link"> <i class="fa fa-exchange"></i> 
                    &nbsp;Transaction History</a>
                  </li>
                  @endif
                 
                </ul>
              </div>
            </div>
          </div>
        </div>
 @yield('content')
      </div>
  
      <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-auto ml-lg-auto">
              <div class="row align-items-center">
                <div class="col-auto">
                  <ul class="list-inline list-inline-dots mb-0">
                    
                    <li class="list-inline-item"><a href="#">FAQ</a></li>
                  </ul>
                </div>
                
              </div>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
              Copyright © <?php echo date('Y');?>  Developed by <a href="https://redxtechnosoft.com/" target="_blank">Redx Technosoft</a> All rights reserved.
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
     <script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
<!-- Session Messages -->
        @if(session()->has('success'))
        <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
        </script>
        @endif
        @if(session()->has('error'))
        <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
        </script>

        @endif 
</html>
    
