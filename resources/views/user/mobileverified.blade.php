<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
     <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Mobile Number Verification | MLM</title>
    <link href="{{asset('user_dashboard_css/css/dashboard.css')}}" rel="stylesheet" />

          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />
  </head>
  <body class="">
    <div class="page">
      <div class="page-single">
        <div class="container">
         <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <img src="{{asset('user_dashboard_css/images/logo.png')}}" class="h-8" alt="">
              </div>
           

              <div class="card">
                    <div class="card-header font-weight-bold">{{ __('Your Mobile number is not verified Yet !') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{route('mobile_verified')}}">
                            @csrf

                            <div class="form-group">
                                <label for="email">{{ __('Mobile No') }}</label>

                                    <input id="mobile" type="number" class="form-control" name="mobile" value="{{old('mobile')}}" required autofocus>

                            </div>



                            <div class="form-group otp">
                                <label for="otp" >OTP</label>
                                    <input id="otp" type="number" class="form-control" name="otp" >
                            </div>
                            <div class="form-group row mb-0 otp submit">
                            <!-- Submit button  -->
                            </div>
                        </form>
                        <div class="form-group send-otp">
                           
                                <button class="btn btn-success" onclick="sendOtp()">Send OTP</button>
                            
                        </div>
                    </div>
                </div>
             <script>
        $('.otp').hide();
        function sendOtp() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
              var mobile = $('#mobile').val();
               
                    if(mobile==" " ||  mobile=="" || mobile==null)
                    { 
                    $("#mobile").addClass("is-invalid");

                    }
                    else{
                         $("#mobile").addClass("is-valid");
                         $("#mobile").removeClass("is-invalid");
                         
                        $.ajax({
                url:'{{route("sendOtp")}}',
                type:'post',
                data: {mobile:mobile},
                success:function(data) {
                     //alert(data);
                    if(data[0] == 1){
                        $('.otp').show();
                        $('#verification').removeClass("d-none");
                        $('.send-otp').hide();
                        $(".submit").append('<div class="col-md-8 offset-md-4"><button type="submit" class="btn btn-primary">{{ __('Submit') }}</button> </div>');
                         toastr.success(data[1])
            
                    }else if(data[0] == 0){
                       toastr.error(data[1])
                        
                    }

                },
                error:function () {
                    console.log('error');
                }
            });
                    }


            
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
    <!-- Session Messages -->
        @if(session()->has('success'))
        <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
        </script>
        @endif
        @if(session()->has('error'))
        <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
        </script>

        @endif 
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
  
</html>