@extends('user.layouts.app')

@section('content')
 @if($user->status == 0)
            <script>
            window.location.href="<?php echo route('home')?>";
            </script>
            @else
                <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              <div class="col-lg-3">
                
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title font-weight-bold">Withdraw Request</h3>

                  </div>
                  <div class="card-body">

                    <form method="post" action="{{route('withdraw.request')}}">
                      @csrf
                  
                    
                        <div class="form-group">
                        <label class="form-label">Withdraw Amount</label>
                        <input class="form-control" placeholder="Amount" name="amount" type="number"/>
                         <span style="color:orange"> @error('amount') {{ $message }}@enderror </span>
                      </div>
                      
                      <div class="form-footer">
                        @if($user->earning == 0)
                        <div class="alert alert-info" role="alert">
                        Your Earning is not enough to raise a withdraw request...
                        </div>
                        @else
                        <button type="submit" class="btn btn-primary btn-block">Send Withdraw Request</button>
                        @endif
                        <br />
                         <h5 class="m-0">
                          Balance: <i class="fa fa-inr fa-sm" style="font-size:12px;"></i> {{$user->earning}} </h5>
                      <small class="text-muted">Un-Released Wallet: &nbsp;<i class="fa fa-inr fa-sm" style="font-size:12px;"></i>&nbsp;{{$user->earning_block}}</small>
                      </div>
                    </form>

                  </div>
                </div>
              </div>
               <div class="col-lg-9">
              <div class="row">
                @forelse($withdraw as $withdraw_request)
              <div class="col-sm-6 col-lg-3">
                <div class="card">
                  <div class="card-body text-center">
                    <div class="card-category">{{date('d M Y ', strtotime($withdraw_request->created_at))}}
                      <br/>
                      {{date('h:i s A', strtotime($withdraw_request->created_at))}}</div>
                    
                    <div class="display-3 my-4">
                      <i class="fa fa-inr"></i>
                      {{$withdraw_request->amount}}
                    </div>
                    <div class="text-center mt-6">
                      @if($withdraw_request->status == 0)
                      <span class="btn btn-warning btn-block">Pending</span>
                      @elseif($withdraw_request->status ==1)
                      <span class="btn btn-success btn-block">Request Accepted</span>
                      @elseif($withdraw_request->status==2)
                       <span class="btn btn-danger btn-block">Request Declined</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              @empty
           <div class="col-sm-12 mt-5 text-center">
            <h4>No Withdrawal Request Found ! </h4>
          </div>
             @endforelse
            <div class="col-sm-12"> {{ $withdraw->links()}}</div>
            </div>
              </div>
            </div>
             
            </div>
          </div>
          @endif
        </div>
@endsection
