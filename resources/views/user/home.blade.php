@extends('user.layouts.app')

@section('content')
         <div class="my-3 my-md-5">
          <div class="container">
            <div class="page-header">
              <h1 class="page-title">
                Dashboard
              </h1>
            </div>
            @if($user->status == 0)
            <div class="row row-cards">
            <div class="col-sm-2 "> </div>
        
            <div class="col-sm-8 ">
            <div class="alert alert-danger" > 
            <i class="fe fe-bell"></i>
            Notice!
            <br />
            Your account is not active, please complete the payment.
            </div> 
            <form action="/razorpay" method="POST" class="mx-auto">
              @csrf

              <script
              src="https://checkout.razorpay.com/v1/checkout.js"
              data-key="rzp_live_rk8040rquAcPfF"
              data-amount="{{$order->amount}}" 
              data-currency="INR"
              data-order_id="{{$order->id}}"
              data-buttontext="Pay Registration Fees"
              data-name="Accure Vision"
              data-description="Registration Fees"
              data-prefill.name="{{Auth::user()->name}}"
              data-prefill.email="{{Auth::user()->email}}"
              data-prefill.contact="{{Auth::user()->mobile}}"
              data-theme.color="#467fcf"
              ></script>
              <input type="hidden" custom="Hidden Element" name="hidden" />
              </form>  
            </div> 

                <div class="col-sm-2 "></div>
            </div>
            @else
            <div class="row row-cards">
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-blue mr-3">
                      <i class="fa fa-handshake-o"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">{{$allreferral_count}}
                        &nbsp;<small>Referrals</small></a></h4>
                      <small class="text-muted">Your Total Referrals</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-green mr-3">
                      <i class="fa fa-handshake-o"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">{{$referralpersonalcount}} <small>Referrals</small></a></h4>
                      <small class="text-muted">Personal referrals</small>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-red mr-3">
                      <i class="fa fa-money"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)"><i class="fa fa-inr fa-sm"></i> {{$user->earning}} <small>Earning</small></a></h4>
                      <small class="text-muted">Un-Released Wallet: &nbsp;<i class="fa fa-inr fa-sm"></i>{{$user->earning_block}}</small>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-red mr-3">
                      <i class="fa fa-money"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)"><i class="fa fa-inr fa-sm"></i> {{$withdrawal}} <small>Total withdraw</small></a></h4>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                      <thead>
                        <tr>
                          <th class="text-center w-1"><i class="icon-people"></i></th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>
                          Status
                          <a href="{{route('referral.index')}}" class="btn btn-sm btn-info float-right"> View all</a>
                          </th>
                         
                         <!--  <th class="text-center"><i class="icon-settings"></i></th> -->
                        </tr>
                      </thead>
                      <tbody>
                        @forelse($referralusers as $personal)
                      <tr>
                          <td class="text-center">
                           <!--  <div class="avatar d-block" style="background-image: url(demo/faces/female/26.jpg)">
                              <span class="avatar-status bg-green"></span>
                            </div> -->
                            <i class="fe fe-user" style="font-size:32px;"></i>
                          </td>
                          <td>
                            <div>{{$personal->name}}</div>
                            <div class="small text-muted">
                              Registered at  
                              <b>{{date('d M Y ', strtotime($personal->created_at))}}</b>
                            </div>
                          </td>
                          <td>
                            <div>
                              <span class="emailProtected{{$personal->id}}">{{$personal->email}}</span>
                            </div>
                            <script>

                                $(document).ready(function(){
                                var avg, splitted, part1, part2;
                                var email= $(".emailProtected{{$personal->id}}").text();
                                splitted = email.split("@");
                                part1 = splitted[0];
                                avg = part1.length / 2;
                                part1 = part1.substring(0, (part1.length - avg));
                                part2 = splitted[1];
                                $(".emailProtected{{$personal->id}}").text(part1 + "...@" + part2);
                                });

                              </script>
                          </td> 
                          <td>
                        @if($personal->status == 0)
                          <span class="badge badge-danger btn-sm">Payment Due</span>
                        @if($user->earning >= 2100 )
                        <span class="btn btn-primary btn-sm makepayment" activation="{{$personal->id}}" onclick="return confirm('Are you sure you want to activate this user?');">Make Payment for <i class="text-uppercase"> {{$personal->name}}</i> </span>
                        @else
                        <span class="btn btn-primary btn-sm lesspayment" >Make Payment for <i class="text-uppercase"> {{$personal->name}}</i> </span>
                        @endif
                        @elseif($personal->status == 1)
                        <span class="badge badge-success">Activated</span>
                        @else
                        @endif
                          </td>
                          
                        </tr>
                        @empty
                         <tr><td colspan="4"><h4 class="text-center"> No Record Found</h4></td></tr>
                        @endforelse
                        
                         <tr><td colspan="4"><a href="{{route('referral.index')}}" class="btn btn-sm btn-info float-right text-uppercase"> View all</a></td></tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
@endif
          </div>
        </div>
        <script>
    $(".lesspayment").click(function() {
      toastr.error("Sorry, You do not have enough balance to activate this user")
      });
    // less balance error

    //make payment

            $(".makepayment").click(function() {
            var user=$('.makepayment').attr('activation');
            $('.makepayment').hide();
            var token = $("input[name='_token']").val();
            $.ajax({
            url: "<?php echo route('activateuser') ?>",
            method: 'POST',
            data: {activation:user,_token:token},
            success: function(data) {
              if(data == 1)
              {
                toastr.success("Registration Fees Submited Successfully By you");
                $("#DisplayReferrals").html(data.html);
                setTimeout(function () {
                window.location = "{{route('home')}}";
                }, 2000);
              }
              else
              {
                toastr.error("Sorry, You do not have enough balance to activate this user");
                setTimeout(function () {
                window.location = "{{route('home')}}";
                }, 2000);
              }
         
            }
            });
            });
        </script>
@endsection
