  @forelse($referralsGet as $personal)
                      <tr>
                          <td class="text-center">
                           <!--  <div class="avatar d-block" style="background-image: url(demo/faces/female/26.jpg)">
                              <span class="avatar-status bg-green"></span>
                            </div> -->
                            <i class="fe fe-user" style="font-size:32px;"></i>
                          </td>
                          <td>
                            <div>{{$personal->name}}</div>
                            <div class="small text-muted">
                              Registered at  
                              <b>{{date('d M Y ', strtotime($personal->created_at))}}</b>
                            </div>
                          </td>
                          <td>
                            <div>
                              <span class="emailProtected{{$personal->id}}">{{$personal->email}}</span>
                            </div>
                            <script>

                                $(document).ready(function(){
                                var avg, splitted, part1, part2;
                                var email= $(".emailProtected{{$personal->id}}").text();
                                splitted = email.split("@");
                                part1 = splitted[0];
                                avg = part1.length / 2;
                                part1 = part1.substring(0, (part1.length - avg));
                                part2 = splitted[1];
                                $(".emailProtected{{$personal->id}}").text(part1 + "...@" + part2);
                                });

                              </script>
                          </td> 
                          <td>
                        @if($personal->status == 0)
                        <span class="badge badge-danger">Payment Due</span>
                        @elseif($personal->status == 1)
                        <span class="badge badge-success">Activated</span>
                        @else
                        @endif
                          </td>
                        </tr>
                        
                        @empty
                         <tr><td colspan="4"><h4 class="text-center"> No Referrals Found Yet !</h4></td></tr>
                        @endforelse