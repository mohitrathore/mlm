@extends('user.layouts.app')

@section('content')
 @if($user->status == 0)
            <script>
            window.location.href="<?php echo route('home')?>";
            </script>
            @else
                <div class="my-3 my-md-5">
          <div class="container">
           
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                      <thead>
                        <tr>
                          <th class="text-center w-1"><i class="icon-people"></i></th>
                          <th>Date</th>
                          <th>Transaction ID</th>
                          <th class="text-center">Purpose</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($transactions as $transaction)
                        <tr>
                          <td class="text-center">
                            #
                          </td>
                          <td>
                            
                            <div class="text-muted">
                              Registered at {{date('d M Y ', strtotime($transaction->created_at))}}
                            </div>
                          </td>
                          <td>
                            <div class="clearfix">
                              <div class="float-left">
                                <strong>{{$transaction->transaction_id}}</strong>
                              </div>
                              
                            </div>
                            
                          </td>
                          <td class="text-center">
                            {{$transaction->purpose}}
                          </td>
                          <td>
                           
                            <div><i class="fa fa-inr"></i>{{$transaction->amount}}</div>
                          </td>
                          
                        </tr>
                        @endforeach
                        <tr>
                        <td colspan="5">{{$transactions->links()}} </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endif
        </div>
@endsection
