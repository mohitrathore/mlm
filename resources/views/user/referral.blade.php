@extends('user.layouts.app')

@section('content')
         <div class="my-3 my-md-5">
          <div class="container">
            <div class="page-header">
              <span class="page-title">
                Referrals
              </span>
            
              
            </div>
            @if($user->status == 0)
            <script>
            window.location.href="<?php echo route('home')?>";
            </script>
            @else
              <!-- Select to use referrals personal to specific person -->
               <div class="form-group" style="width:100%;">
              <!-- <label for="sel1">Select list:</label> -->
              <select class="form-control text-capitalize" id="referrals">
              <option> -- Select Particular user to see him/her referrals -- </option>
              <option value="0"> All Referrals</option>
              @foreach($allreferrals_selection as $referral)
               <option  value="{{$referral->id}}"> {{$referral->name}}</option>
              @endforeach
              
              </select>
              </div>
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                      <thead>
                        <tr>
                          <th class="text-center w-1"><i class="icon-people"></i></th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>
                          Status
                         
                          </th>
                         
                         <!--  <th class="text-center"><i class="icon-settings"></i></th> -->
                        </tr>
                      </thead>
                      <tbody id="DisplayReferrals">
                        @forelse($allreferralusers as $personal)
                      <tr>
                          <td class="text-center">
                           <!--  <div class="avatar d-block" style="background-image: url(demo/faces/female/26.jpg)">
                              <span class="avatar-status bg-green"></span>
                            </div> -->
                            <i class="fe fe-user" style="font-size:32px;"></i>
                          </td>
                          <td>
                            <div>{{$personal->name}}</div>
                            <div class="small text-muted">
                              Registered at  
                              <b>{{date('d M Y ', strtotime($personal->created_at))}}</b>
                            </div>
                          </td>
                          <td>
                            <div>
                              <span class="emailProtected{{$personal->id}}">{{$personal->email}}</span>
                            </div>
                            <script>

                                $(document).ready(function(){
                                var avg, splitted, part1, part2;
                                var email= $(".emailProtected{{$personal->id}}").text();
                                splitted = email.split("@");
                                part1 = splitted[0];
                                avg = part1.length / 2;
                                part1 = part1.substring(0, (part1.length - avg));
                                part2 = splitted[1];
                                $(".emailProtected{{$personal->id}}").text(part1 + "...@" + part2);
                                });

                              </script>
                          </td> 
                          <td>
                        @if($personal->status == 0)
                        <span class="badge badge-danger">Payment Due</span>
                        @elseif($personal->status == 1)
                        <span class="badge badge-success">Activated</span>
                        @else
                        @endif
                          </td>
                        </tr>
                        
                        @empty
                         <tr><td colspan="4"><h4 class="text-center"> No Record Found</h4></td></tr>
                        @endforelse
                       
                        <tr><td colspan="4">{{ $allreferralusers->links()}}</td></tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
@endif
          </div>
        </div>
<script>

  $("#referrals").change(function(){
        var referralid= $(this).val();
      
        if( referralid == 0){
            window.location.href="/referrals";
        }
        else
        {
         var token = $("input[name='_token']").val();
          $.ajax({
          url: "<?php echo route('referral.find') ?>",
          method: 'POST',
          data: {referralid:referralid,_token:token},
          success: function(data) {

          $("#DisplayReferrals").html(data.html);
          }
          });
        }
        
      }); //Referrals filter
    </script>
@endsection
