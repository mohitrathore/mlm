@extends('admin.layouts.app')

@section('content')
<div class="my-3 my-md-5">
          <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Admin Dashboard
              </h1>
            </div>
            <div class="row row-cards">
              
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-green mr-3">
                      <i class="fe fe-users"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">{{$RegisteredUsercount}} <small>Registered</small></a></h4>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-green mr-3">
                      <i class="fa fa-handshake-o"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">{{$MemberUsercount}} <small>Members</small></a></h4>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-lg-6">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-orange mr-3">
                      <i class="fa fa-money"></i>
                    </span>
                    <div> 
                      <h4 class="m-0"><a href="javascript:void(0)">
                        <i class="fa fa-inr" style="font-size:15px;"></i>
                        {{$MemberUsercount * 2100}} /- <small>Total Earning <small>(Registration Amount)</small></small></a></h4>
                      <small class="text-muted">
                        <i class="fa fa-inr"></i>
                      {{$MemberUsercount * 956.8}} /- Future Paid to Members <small>(Referral Amount)</small>
                    </small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
             <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="table-responsive">
                    <h6 class="page-title m-5" style="font-size: 1.125rem;">
                    Transactions (10)
                    <a href="{{route('admin.transaction')}}" class="btn btn-primary btn-sm float-right">ViEW ALL</a>
                    </h6>
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                    <thead>
                    <tr>
                    <th class="text-center w-1"><i class="icon-people"></i></th>
                    <th>Date</th>
                    <th>Email</th>
                    <th>Transaction ID</th>
                    <th class="text-center">Purpose</th>
                    <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($transactions as $transaction)
                    <tr>
                    <td class="text-center">
                    {{ $no++ }}
                    </td>
                    <td>
                    <div class="text-muted">
                    Transaction at {{date('d M Y - h i s A', strtotime($transaction->created_at))}}
                    </div>
                    </td>
                    <td>
                    
                    @if($transaction->email == $user->email)
                    <h6 class="text-center">You</h6>
                    @else 
                    <div>
                    <span class="emailtran{{$transaction->id}}">
                    {{$transaction->email}}
                    </span>
                    </div>
                    @endif
                    </td>
                    <td>
                    <div class="clearfix">
                    <div class="float-left">
                    <strong>{{$transaction->transaction_id}}</strong>
                    </div>

                    </div>
                    </td>
                    <td class="text-center">
                    {{$transaction->purpose}}
                    </td>
                    <td>

                    <div>
                    <i class="fa fa-inr" style="font-size:12px;"></i> 
                    {{$transaction->amount}}
                     @if($transaction->purpose == 'Withdraw')
                            <i class="fa fa-arrow-up text-danger" aria-hidden="true"></i>
                            @else 
                             <i class="fa fa-arrow-down text-success" aria-hidden="true"></i>
                            @endif
                    </div>
                    </td>

                    </tr>
                    @empty
                    <tr>
                    <td colspan="5"> No Trasaction Found</td>
                    </tr>
                    @endforelse
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
           
              
              <div class="col-sm-6 col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">Withdrawal Requests (5) 
                     <a href="{{route('admin.withdraw')}}" class="btn btn-primary text-white btn-sm ml-5">ViEW ALL</a>
                   </h4>
                  </div>
                  <table class="table card-table">
                    @forelse($withdrawal as $withdrawal_entries)
                    <tr>
                    <td width="1">#</td>
                    <td>{{$withdrawal_entries->email}}</td>
                    <td><i class="fa fa-inr" style="font-size: 13px;"></i>
                      {{$withdrawal_entries->amount}}</td>
                    <td class="text-right"> 
                      @if($withdrawal_entries->status ==0)
                             
                             <span class="badge badge-warning">Pending</span>
                             @elseif($withdrawal_entries->status ==1)
                             <span class="badge badge-success ">Verified</span>
                             @elseif($withdrawal_entries->status == 2)
                             <span class="badge badge-danger">Declined</span>
                             @endif
                    </td>
                    </tr>
                    @empty
                    <tr>
                    <td colspan="4" class="text-center font-weight-bold mt-5">No Withdrawal Request Found  </td>

                    </tr>
                    @endforelse
                  </table>
                </div>
              </div>
            
              <div class="col-md-6 col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Recent Members (10)
                     <a href="#" class="btn btn-primary text-white btn-sm ml-5">ViEW ALL</a></h3>
                  </div>
                  <div class="card-body o-auto" style="height: 15rem">
                    @forelse($members as $member)
                    <ul class="list-unstyled list-separated">
                      <li class="list-separated-item">
                        <div class="row align-items-center">
                          <div class="col-auto">
                            @if($member->profileImage !== null)

                            <span class="avatar" style="background-image: url(../ProfileImages/{{$member->profileImage}})"></span>
                            @else
                            <i class="fe fe-user mr-5" style="font-size:30px;"></i>
                            @endif
                          </div>
                          <div class="col">
                            <div>
                              <a href="javascript:void(0)" class="text-inherit">{{$member->name}}</a>
                            </div>
                            <small class="d-block item-except text-sm text-muted h-1x">{{$member->email}}</small>
                          </div>
                          <div class="col-auto">
                           Registered at {{date('d M Y | h:i:s A', strtotime($member->created_at))}}
                          </div>
                        </div>
                      </li>
                 
                    </ul>

                    @empty
                      <ul class="list-unstyled list-separated">
                      <li class="list-separated-item">
                        <div class="row align-items-center">
                          <div class="col-12 text-center">
                           <b>No Registered Members Found Yet !</b>
                          </div>
                          
                        </div>
                      </li>
                    </ul>
                    @endforelse
                    
                  </div>
                </div>
              </div>
            
            </div>
          </div>
        </div>
@endsection
