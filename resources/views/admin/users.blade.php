@extends('admin.layouts.app')
 
@section('content')
                <div class="my-3 my-md-5">
          <div class="container">
           
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="page-header">
              <h1 class="page-title container-fluid">
                <i class="fe fe-user"></i>  Members 
                 <form class="input-icon my-3 my-lg-0 float-right">
                   <input  id="myInput" type="text" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search" style="font-size: 20px;"></i>
                  </div>
                </form>
              </h1>
            </div>
                <div class="card">
                  <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                      <thead>
                        <tr>
                          <th class="text-center w-1"><i class="icon-people"></i></th>
                          <th>Name</th>
                          <th>Referrals</th>
                          <th>Wallet</th>
                          <th class="text-center">status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="myTable">
                        @forelse($members as $member)
                      <tr>
                          <td class="text-center">
                            {{$no++}}
                          </td>
                          <td>
                            <a href="javascript:void(0)" class="text-inherit text-capitalize">{{$member->name}}</a>
                            <small class="d-block item-except text-sm text-muted h-1x">{{$member->email}}</small>
                          </td>
                          <td>
                           {{$member->referral_count}} Times
                          </td>
                          <td>
                          
                            <a href="javascript:void(0)" class="text-inherit">
                               <i class="fa fa-inr fa-sm"></i>
                            {{$member->earning}} /- (Earning)
                            </a>
                            <small class="d-block item-except text-sm text-muted h-1x"> 
                              <i class="fa fa-inr fa-sm"></i>
                              {{$member->earning_block}} (Un-Released)</small>
                          </td>
                          <td  class="text-center"> 
                            @if($member->status == 0)
                            <span class="badge badge-warning btn-sm">Payment Due</span>
                            @elseif($member->status == 1)
                            <span class="badge badge-success">Activated</span>
                            @else
                            @endif
                          </td>
                          </td>
                          <td>
                                <!-- Deposit Modal -->
                                <div class="modal fade" id="depositModal{{$member->id}}" tabindex="-1" role="dialog" aria-labelledby="depositlabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <form method="post" action="{{route('admin.users.deposit')}}">
                                  @csrf
                                <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Deposit Money to Wallet</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                               
                                </button>
                                </div>
                                <div class="modal-body">
                               
                                <div class="form-group">
                                <label for="recipient-name" class="col-form-label text-capitalize">Recipient:</label>
                                <input type="text" class="form-control text-capitalize" id="recipient-name" value="{{$member->name}}" >
                                </div>
                                <div class="form-group">
                                <label for="message-text" class="col-form-label">Amount:</label>
                                <input type="number" name="amount" class="form-control" required>
                                </div>
                               
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 <input type="hidden" name="email" value="{{$member->email}}">
                                <button type="submit" class="btn btn-primary">Proceed to Deposit</button>
                                </div>
                                </div>
                                 </form>
                                </div>
                                </div>
                                 <!-- Withdraw Modal -->
                                <div class="modal fade" id="withdrawModal{{$member->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <form method="post" action="{{route('admin.users.withdraw')}}">
                                  @csrf
                                <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Withdraw  Money From Wallet</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                </button>
                                </div>
                                <div class="modal-body">

                                <div class="form-group">
                                <label for="recipient-name" class="col-form-label ">Recipient:</label>
                                <input type="text" class="form-control text-capitalize" id="recipient-name" value="{{$member->name}}">
                                </div>
                                <div class="form-group">
                                <label for="message-text" class="col-form-label">Amount:</label>
                                <input type="number" name="amount" class="form-control" required>
                                </div>

                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <input type="hidden" name="email" value="{{$member->email}}">
                                <button type="submit" class="btn btn-primary">Proceed to Deposit</button>
                                </div>
                                </div>
                                </form>
                                </div>
                                </div>

                           <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                              Action
                              </button>
                              <div class="dropdown-menu">
                              <a class="dropdown-item text-success" href="{{route('admin.users.active',$member->id)}}">
                              <i class="fa fa-check-circle" aria-hidden="true"></i>
                              &nbsp;
                              Active</a>
                              
                              <a class="dropdown-item text-primary" href="#" data-toggle="modal" data-target="#depositModal{{$member->id}}">
                                <i class="fa fa-plus " aria-hidden="true"></i>
                                &nbsp;
                              Deposit Money</a>
                              <a class="dropdown-item text-info" href="#" data-toggle="modal" data-target="#withdrawModal{{$member->id}}">
                                <i class="fa fa-minus " aria-hidden="true"></i>
                                &nbsp;
                              Withdrawal Money</a>
                                     
                              </div>
                          </td>
                         
                        </tr>
                        @empty
                        <tr>
                           <td colspan="6" class="text-center">
                           No Members Register Yet !
                          </td>
                         
                        </tr>
                        @endforelse
                        <tr>
                           <td colspan="6" class="text-center">
                          {{$members->links()}}
                          </td>
                         
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
