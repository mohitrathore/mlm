@extends('admin.layouts.app')

@section('content')
                <div class="my-3 my-md-5">
          <div class="container">
           
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="page-header">
              <h1 class="page-title container-fluid">
                <i class="fa fa-money"></i> Withdrawals
                 <form class="input-icon my-3 my-lg-0 float-right">
                   <input  id="myInput" type="text" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search" style="font-size: 20px;"></i>
                  </div>
                </form>
              </h1>
            </div>
                <div class="card">

                  <div class="table-responsive">
                   
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table" >
                      <thead>
                        <tr>
                          <th class="text-center w-1"><i class="icon-people"></i></th>
                          <th>Date</th>
                          <th>User</th>
                          <th>Transaction ID</th>
                          <th>Amount</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="myTable">
                        @foreach($withdraws as $withdraw)
                        <tr>
                          <td class="text-center">
                            #
                          </td>
                          <td>
                             
                            @if($withdraw->email == $user->email)
                            <h6 class="text-center">You</h6>
                            @else 
                            <div>
                            <span class="emailtran{{$withdraw->id}}">
                            {{$withdraw->email}}
                            </span>
                            </div>
                            @endif

                            
                          </td>
                          <td>
                            <div class="clearfix">
                              <div class="float-left">
                                <strong>{{$withdraw->transaction_id}}</strong>
                              </div>
                              
                            </div>
                            
                          </td>
                          <td>
                            
                            <div class="text-muted">
                              Requested at {{date('d M Y ', strtotime($withdraw->created_at))}}
                            </div>
                          </td>
                          
                          <td>
                           
                            <div>
                              <i class="fa fa-inr"></i>
                              {{$withdraw->amount}}
                          </div>
                          </td>
                          <td>
                             @if($withdraw->status ==0)
                             <span class="badge badge-warning">Pending</span>
                             @elseif($withdraw->status ==1)
                             <span class="badge badge-success">Verified</span>
                             @elseif($withdraw->status == 2)
                             <span class="badge badge-danger">Declined</span>
                             @endif

                             <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                              Action
                              </button>
                              <div class="dropdown-menu">
                              <a class="dropdown-item text-success" href="{{route('admin.withdraw.approve',$withdraw->id)}}">
                              <i class="fa fa-check-circle" aria-hidden="true"></i>
                              &nbsp;
                              Approve</a>
                              <a class="dropdown-item text-warning" href="{{route('admin.withdraw.pending',$withdraw->id)}}">
                               <i class="fa fa-clock-o " aria-hidden="true"></i>
                               &nbsp;
                              Pending</a>
                              <a class="dropdown-item text-danger" href="{{route('admin.withdraw.denied',$withdraw->id)}}">
                                <i class="fa fa-ban " aria-hidden="true"></i>
                                &nbsp;
                              Denied</a>
                              </div>
                          </td>
                          
                        </tr>
                        @endforeach
                        <tr>
                        <td colspan="6">{{$withdraws->links()}} </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
@endsection
