@extends('admin.layouts.app')

@section('content')
                <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              <div class="col-lg-4">
                 
                <div class="card">
                  <div class="card-body">
                    <div class="media">

                      @if($user->profileImage !== null)
              
                    <span class="avatar avatar-xxl mr-5" style="background-image: url(../ProfileImages/{{$user->profileImage}})"></span>
                      @else
                       <i class="fe fe-user mr-5" style="font-size:40px;"></i>
                      @endif
                      <div class="media-body">
                        <h3 class="m-0 text-capitalize">{{$user->name}}</h3>
                        <p class="text-muted mb-0 text-capitalize">{{Session::get('level_name')}}</p>
                          
                        <h6 class="mt-2">
                        Total: 
                        <i class="fa fa-inr" style="font-size:12px;"></i>
                         {{$MemberUsercount * 2100}} /- 
                        </h6>  
                        <h6 class="mt-1">
                        Paid to Users: <i class="fa fa-inr" style="font-size:12px;"></i>
                       {{$MemberUsercount * 956.8}} /- 
                        </h6>  
                        <h6 class="mt-1">
                        Actual Balance: <i class="fa fa-inr" style="font-size:12px;"></i>
                        {{ $MemberUsercount * 2100 - $MemberUsercount * 956.8}} /- 
                        </h6>
                     
                        <!-- <i class="fa fa-inr"></i>
                      {{$MemberUsercount * 956.8}} /- Future Paid to Members <small>(Referral Amount)</small> -->
                      </div>
                    </div>
                  </div>
                </div>
             
              </div>
              <div class="col-lg-8">
             
                <form class="card" method="post" enctype="multipart/form-data" action="{{route('admin.profile.update')}}">
                  @csrf
                  <div class="card-body">
                    <h3 class="card-title">Edit Profile</h3>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Name</label>
                          <input type="text" class="form-control" name="name" placeholder="Name" value="{{$user->name}}">

                          <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Mobile</label>
                          <input type="text" class="form-control" name="mobile" placeholder="Mobile" value="{{$user->mobile}}">
                          <span style="color:orange"> @error('mobile') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-7">
                        <div class="form-group">
                          <label class="form-label">Email address</label>
                          <input type="email" class="form-control" name="email" placeholder="Email" value="{{$user->email}}" disabled="disabled">
                        </div>
                      </div>

                      <div class="col-sm-6 col-md-5">
                        <div class="form-group">
                          <label class="form-label">Profile Picture</label>
                          <input type="file" class="form-control" name="profileImage" placeholder="Image">
                        </div>
                      </div>
                      
                     
                     
                      <div class="col-md-12">
                        <div class="form-group mb-0">
                          <label class="form-label">Address</label>
                          <textarea rows="5" class="form-control" name="address" placeholder="Here can be your address" >{{$user->address}}</textarea>
                          <span style="color:orange"> @error('address') {{ $message }}@enderror </span>
                        </div>
                      </div>
                       <input type="hidden" class="form-control" name="oldprofileImage" value="{{$user->profileImage}}"> 
                    </div>
                  </div>
                  <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Update Profile</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
@endsection
