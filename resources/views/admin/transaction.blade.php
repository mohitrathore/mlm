@extends('admin.layouts.app')

@section('content')
                <div class="my-3 my-md-5">
          <div class="container">
           
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="page-header">
              <h1 class="page-title container-fluid">
                <i class="fa fa-exchange"></i>  Transactions 
                 <form class="input-icon my-3 my-lg-0 float-right">
                   <input  id="myInput" type="text" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search" style="font-size: 20px;"></i>
                  </div>
                </form>
              </h1>
            </div>
                <div class="card">
                  <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                      <thead>
                        <tr>
                          <th class="text-center w-1"><i class="icon-people"></i></th>
                          <th>Date</th>
                          <th>User Email</th>
                          <th>Transaction ID</th>
                          <th class="text-center">Purpose</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody id="myTable">
                        @forelse($transactions as $transaction)
                        <tr>
                          <td class="text-center">
                            #
                          </td>
                         
                          <td>
                            
                            <div class="text-muted">
                              Registered at {{date('d M Y ', strtotime($transaction->created_at))}}
                            </div>
                          </td>
                           <td>
                             
                            @if($transaction->email == $user->email)
                            <h6 class="text-center">You</h6>
                            @else 
                            <div>
                            <span class="emailtran{{$transaction->id}}">
                            {{$transaction->email}}
                            </span>
                            </div>
                            @endif
                          </td>
                          <td>
                            <div class="clearfix">
                              <div class="float-left">
                                <strong>{{$transaction->transaction_id}}</strong>
                              </div>
                              
                            </div>
                            
                          </td>
                          <td class="text-center">
                            {{$transaction->purpose}}
                          </td>
                          <td>
                           
                            <div>
                              <i class="fa fa-inr"></i>
                              {{$transaction->amount}}
                              @if($transaction->purpose == 'Withdraw')
                            <i class="fa fa-arrow-up text-danger" aria-hidden="true"></i>
                            @else 
                             <i class="fa fa-arrow-down text-success" aria-hidden="true"></i>
                            @endif
                          </div>
                          </td>
                          
                        </tr>
                        @empty
                        <tr>
                          <td colspan="6" class="text-center">
                           <b>No Transaction Found Yet !</b>
                          </td>
                        </tr>
                        @endforelse
                        <tr>
                        <td colspan="6">{{$transactions->links()}} </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
