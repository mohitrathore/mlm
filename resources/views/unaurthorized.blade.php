<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    
    <title>Accure Vision | Unaurtorized Page </title>
    
    <!-- Dashboard Core -->
    <link href="{{asset('user_dashboard_css/css/dashboard.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="">
    <div class="page">
      <div class="page-content">
        <div class="container text-center">
          <div class="display-1 text-muted mb-5"><i class="si si-exclamation"></i> 401</div>
          <h1 class="h2 mb-3">Oops.. You cannot access this page! This is for only  <b>'{{$role}}'</b>&hellip;</h1>
          <br />
          <a class="btn btn-primary" href="javascript:history.back()">
            <i class="fa fa-arrow-left mr-2"></i>Go back
          </a>
        </div>
      </div>
    </div>
  </body>
</html>