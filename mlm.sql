-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2021 at 06:04 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mlm`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankaccounts`
--

CREATE TABLE `bankaccounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bankholder_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bankaccounts`
--

INSERT INTO `bankaccounts` (`id`, `email`, `bankholder_name`, `account_number`, `bank_name`, `ifsc_code`, `created_at`, `updated_at`) VALUES
(7, 'mohit@gmail.com', 'Mohit Singh Rathore', '0767001500055648', 'Punjab National Bank', 'PUNB0076700', '2021-03-22 04:57:18', '2021-03-22 05:00:07');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_count` int(11) NOT NULL,
  `amount` float NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name`, `member_count`, `amount`, `level`, `created_at`, `updated_at`) VALUES
(1, 'promotor', 5, 600, 1, NULL, NULL),
(2, 'trainer', 25, 200, 2, NULL, NULL),
(3, 'sales manager', 125, 80, 3, NULL, NULL),
(4, 'area manager', 625, 32, 4, NULL, NULL),
(5, 'zonal sales manager', 3125, 16, 5, NULL, NULL),
(6, 'state sales manager', 15625, 3, 6, NULL, NULL),
(7, 'MD for Sales', 78125, 13, 7, NULL, NULL),
(8, 'office management', 390625, 8, 8, NULL, NULL),
(9, 'director advisory board', 1953125, 5, 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2014_10_12_000000_create_users_table', 2),
(5, '2021_03_17_105753_create_transaction_table', 3),
(7, '2021_03_17_113546_create_levels_table', 4),
(9, '2021_10_17_105256_create_bankaccounts_table', 5),
(10, '2021_109_19_66262_create_withdraw_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('mohit@gmail.com', '$2y$10$AKsIPIRlLWwHST3ArM9Pdu7yk.AljJEQsctmHvhmU0EinjOWy2Xpe', '2021-03-16 04:25:54');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purpose` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `email`, `transaction_id`, `purpose`, `amount`, `created_at`, `updated_at`) VALUES
(2, 'user15@gmail.com', 'TRANS5551635', 'Registration Amount', 2100, '2021-03-23 03:51:54', '2021-03-23 03:51:54'),
(3, 'mohit@gmail.com', 'TRANS3499757', 'Referral', 600, '2021-03-23 03:51:55', '2021-03-23 03:51:55'),
(4, 'admin@gmail.com', 'TRANS9009876', 'Referral', 200, '2021-03-23 03:51:55', '2021-03-23 03:51:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profileImage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT 0,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_count` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `earning` float NOT NULL DEFAULT 0,
  `earning_block` float NOT NULL DEFAULT 0,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `otp_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `profileImage`, `address`, `otp`, `level`, `referral_code`, `referral_by`, `referral_count`, `earning`, `earning_block`, `role`, `email_verified_at`, `otp_verified_at`, `password`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '1234567890', 'u20210323070948.27541040_961509874001592_3328431729453291475_n.jpg', 'Admin address', NULL, 0, 'MLM12dfghd', NULL, '1', 0, 1956.8, 'admin', '2021-03-16 18:30:00', '2021-03-16 18:30:00', '$2y$10$Dw3hkpBHijismPUSAHKy4O9T.QlL4q62QBb/uAmKUtiX96d7blXq2', NULL, 1, NULL, '2021-03-23 03:51:55'),
(15, 'mohit singh rathore', 'mohit@gmail.com', '8949842201', 'u20210322091659.IMG_20191119_194053.jpg', 'dqwdqwdqwdqwdw', NULL, 0, 'MLM20880667', 'admin@gmail.com', '5', 4056.8, 1800, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-23 03:51:55'),
(21, 'user2', 'user2@gmail.com', '1111111113', NULL, NULL, NULL, 0, 'MLM2088073', 'user4@gmail.com', '1', 0, 944, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:25'),
(24, 'user4', 'user4@gmail.com', '1111111114', NULL, NULL, NULL, 0, 'MLM2088022', 'mohit@gmail.com', '1', 0, 951.68, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:25'),
(27, 'user5', 'user5@gmail.com', '5555555555', NULL, NULL, NULL, 0, 'MLM2088055', 'user2@gmail.com', '1', 0, 931.2, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:25'),
(28, 'user6', 'user6@gmail.com', '5555666555', NULL, NULL, NULL, 0, 'MLM2088066', 'user5@gmail.com', '1', 0, 928, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:24'),
(29, 'user 7 ', 'user7@gmail.com', '4646464959', NULL, NULL, NULL, 0, 'MLM20880777', 'user6@gmail.com', '1', 0, 912, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:24'),
(30, 'user8', 'user8@gmail.com', '111888813', NULL, NULL, NULL, 0, 'MLM2088088', 'user7@gmail.com', '1', 0, 880, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:24'),
(31, 'user9', 'user9@gmail.com', '1111999914', NULL, NULL, NULL, 0, 'MLM2088099', 'user8@gmail.com', '1', 0, 800, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:24'),
(32, 'user10', 'user10@gmail.com', '5551010555', NULL, NULL, NULL, 0, 'MLM2081010', 'user9@gmail.com', '1', 0, 600, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:24'),
(33, 'user11', 'user11@gmail.com', '5555661111', NULL, NULL, NULL, 0, 'MLM2118111', 'user10@gmail.com', '0', 0, 0, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 03:59:24'),
(36, 'user12', 'user12@gmail.com', '5551012121', NULL, NULL, NULL, 0, 'MLM2081121', 'mohit@gmail.com', '0', 0, 0, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-19 04:45:35'),
(37, 'user13', 'user13@gmail.com', '5553361111', NULL, NULL, NULL, 0, 'MLM2133111', 'mohit@gmail.com', '0', 0, 0, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-17 05:21:44', '2021-03-22 07:53:02'),
(38, 'user15', 'user15@gmail.com', '5551515155', NULL, NULL, NULL, 0, 'MLM2133151', 'mohit@gmail.com', '0', 0, 0, 'user', NULL, '2021-03-16 18:30:00', '$2y$10$AJAIu8DeEbP5k3t5cWt3meCp5fDQhpdHDZbH4blr1ZS8hb4aI0Vn6', NULL, 1, '2021-03-24 05:21:44', '2021-03-23 03:51:54');

-- --------------------------------------------------------

--
-- Table structure for table `withdraw`
--

CREATE TABLE `withdraw` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `withdraw`
--

INSERT INTO `withdraw` (`id`, `email`, `transaction_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(27, 'mohit@gmail.com', 'TRANS3861157', 3000, '2', '2021-03-23 07:17:12', NULL),
(33, 'mohit@gmail.com', 'TRANS1913513', 3000, '0', '2021-03-23 07:23:37', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankaccounts`
--
ALTER TABLE `bankaccounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`);

--
-- Indexes for table `withdraw`
--
ALTER TABLE `withdraw`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankaccounts`
--
ALTER TABLE `bankaccounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `withdraw`
--
ALTER TABLE `withdraw`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
