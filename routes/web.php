<?php
 
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


/* User Routes*/
Route::group([
'prefix'=>'/',
'middleware'=>['mobileverification'],

], function(){
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/activate', 'ActivatedByparentController@activate')->name('activateuser');
Route::get('/referrals', 'ReferralController@index')->name('referral.index');
Route::post('/referralsfind', 'ReferralController@find')->name('referral.find');
Route::get('/profile', 'ProfileController@index')->name('profile.index');
Route::post('/profileupdate', 'ProfileController@profileupdate')->name('profile.update');
Route::post('/account', 'ProfileController@account')->name('profile.account');
Route::get('/withdraw', 'WithdrawController@index')->name('withdraw.index');
Route::post('/withdraw_request', 'WithdrawController@request')->name('withdraw.request');
Route::post('/razorpay', 'HomeController@razorpay')->name('fees');
Route::get('/transaction', 'TransactionController@index')->name('transaction');
});

Route::post('/sendOtp', 'HomeController@sendOtp')->name('sendOtp');
Route::get('/mobilenotverified', 'HomeController@mobile_notverified')->name('mobile_not_verified');
Route::post('/mobileverification', 'HomeController@mobile_verified')->name('mobile_verified');

/* User Routes*/
Route::group([
'prefix'=>'Ur0HOxTCCViZRbFd',
'middleware'=>['auth','admin'],

], function(){
Route::get('/dashboard', 'Admin\HomeController@index')->name('admin.dashboard');
Route::get('/profile', 'Admin\ProfileController@index')->name('admin.profile');
Route::post('/profileupdate', 'Admin\ProfileController@update')->name('admin.profile.update');
Route::get('/transaction', 'Admin\TransactionController@index')->name('admin.transaction');
Route::get('/withdraw', 'Admin\WithdrawController@index')->name('admin.withdraw');
Route::get('/requestapprove/{id}', 'Admin\WithdrawController@approve')->name('admin.withdraw.approve');
Route::get('/requestpending/{id}', 'Admin\WithdrawController@pending')->name('admin.withdraw.pending');
Route::get('/requestdenied/{id}', 'Admin\WithdrawController@denied')->name('admin.withdraw.denied');

Route::get('/members', 'Admin\UsersController@index')->name('admin.users.index');
Route::get('/activatemembers/{id}', 'Admin\UsersController@active')->name('admin.users.active');
Route::post('/deposit', 'Admin\UsersController@deposit')->name('admin.users.deposit');
Route::post('/withdraw', 'Admin\UsersController@withdraw')->name('admin.users.withdraw');

});
