<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('member_count');
            $table->integer('amount');
            $table->integer('level');
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('levels')->insert(
        array(
        'name' => 'promotor',
        'member_count' => 5,
        'amount' => 600,
        'level' => 1,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'trainer',
        'member_count' => 25,
        'amount' => 200,
        'level' => 2,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'sales manager',
        'member_count' => 125,
        'amount' => 80,
        'level' =>3,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'area manager',
        'member_count' => 625,
        'amount' => 32,
        'level' => 4,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'zonal sales manager',
        'member_count' => 3125,
        'amount' => 16,
        'level' => 5,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'state sales manager',
        'member_count' =>15625,
        'amount' => 3.2,
        'level' => 6,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'MD for sales',
        'member_count' => 78125,
        'amount' => 12.8,
        'level' => 7,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'office management',
        'member_count' => 390625,
        'amount' => 7.68,
        'level' => 8,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        array(
        'name' => 'director of advisory board',
        'member_count' => 1953125,
        'amount' => 5.12,
        'level' => 9,
        'created_at' =>now(),
        'updated_at' => now()
        ),
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}
