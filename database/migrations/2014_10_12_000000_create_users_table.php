<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->string('profileImage')->nullable();
            $table->longText('address')->nullable();
            $table->integer('otp')->nullable();
            $table->integer('level')->default(0);
            $table->string('referral')->nullable();
            $table->string('referral_count')->default(0);
            $table->float('earning')->default(0);
            $table->float('earning_block')->default(0);
            $table->string('role')->default('user');
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('otp_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
        // Insert some stuff
        DB::table('users')->insert(
        array(
        'name' => 'Admin',
        'email' => 'mlm@admin.com',
        'mobile' => '1234567890',
        'password' => Hash::make('mlm@admin.123'),
        'role' => 'admin',
        'created_at' =>now(),
        'updated_at' => now(),
        'status' => 1
        )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
